package com.example.serverSide.model;

import lombok.Getter;
import lombok.Setter;

//сущность входящего сообщения с логином
public class InMessage {

    @Setter
    @Getter
    private String login;

}
