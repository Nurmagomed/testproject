package com.example.serverSide.model;


import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;

//Сущность клиента с полями имя, фамилия, сообщение, время.
public class ServerResponce {

    private String body;

    private Date timeStampe;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = "Добрый день, " + body + ", Ваше сообщение успешно обработано!";
    }

    public Date getTimeStampe() {
        return timeStampe;
    }

    public void setTimeStampe(Date timeStampe) {
        this.timeStampe = timeStampe;
    }
}
