package com.example.serverSide.model;
import java.text.SimpleDateFormat;
import java.util.Date;

//Сущность клиента с полями имя, фамилия, сообщение, время.
public class OutMessage {

    private String name;

    private String timestamp;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = "Добрый день, " + name + ", Ваше сообщение успешно обработано!";
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp() {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH24:mm:ss");
        this.timestamp = df.format(date);
    }

}
