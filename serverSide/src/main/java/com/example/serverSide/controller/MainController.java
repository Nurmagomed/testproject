package com.example.serverSide.controller;

import com.example.serverSide.model.InMessage;
import com.example.serverSide.model.OutMessage;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class MainController {

    //тут надо вернуть JSON с сообщением кто подключился
    @RequestMapping(value = "/start", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public InMessage startResponce() {
        InMessage inMessage = new InMessage();

        return inMessage;
    }

    //метод post который возвращает "Добрый день,имя!"
    @RequestMapping(value = "/hello", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public OutMessage messageResponce(String name) {
        OutMessage outMessage = new OutMessage();
        outMessage.setName(name);
        outMessage.setTimestamp();

        return outMessage;
    }

}
