package com.example.webapplication;

import com.example.webapplication.models.LoginMessage;
import com.example.webapplication.models.parametrs.Request;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.awt.*;
import java.io.IOException;

@Controller
public class MainController {

    @GetMapping("/")
    public String greeting() {

        return "sendMessage";
    }

    @PostMapping("/responce")
    public String message() { return  "responce";}

    @PostMapping(value = "/sendJson", produces = "application/json")
    public  String writeJson(@RequestBody String request) {
        JsonObject item = new JsonObject();
        JsonObject user = new JsonObject();
        JsonObject message = new JsonObject();

        //создание json обектра и приствоение полям переменных из формы
        user.addProperty("Name", "Иван");
        user.addProperty("Secondname", "Иванов");
        message.addProperty("Body", "Привет всем!");
        message.addProperty("Timestampe", "Время");


        item.add("User", user);
        item.add("Message", message);

        Gson gson = new Gson();
        String json = gson.toJson(item);

        return writeJson(json);

    }


    }


