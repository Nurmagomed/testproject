package com.example.webapplication.models.parametrs;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.IOException;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class Request {

    @Getter
    User user;
    @Getter
    Message message;

    public Request(String name, String secondName, String body) {
        this.user = new User(name, secondName);
        this.message = new Message(body);
    }

    public static String writeJson(String nameForm, String secondNameForm, String bodyForm) throws IOException {
        JsonObject request = new JsonObject();
        JsonObject user = new JsonObject();
        JsonObject message = new JsonObject();

        user.addProperty("Name", "Иван");
        user.addProperty("Secondname","Иванов");
        message.addProperty("Body","Привет всем!");
        message.addProperty("Timestampe", "Время");
        request.add("User", user);
        request.add("Message", message);

        Gson gson = new Gson();
        String json = gson.toJson(request);

        return json;
    }

}