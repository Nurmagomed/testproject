package com.example.webapplication.models.parametrs;

import lombok.Getter;
import lombok.Setter;

public class SecondName {

//    @Getter
//    @Setter
    String secondName;

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
}
