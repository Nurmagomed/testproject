package com.example.webapplication.models.config;

import lombok.Getter;

import java.io.*;
import java.util.Properties;


//сущность с данными конфигурационного файла
public class MyProperties {

    @Getter
    private String login;
    @Getter
    private String password;
    @Getter
    private String host;

    private Properties prop = new Properties();

    //читает конфигурационный файл и задает значение полям
    public void setProperties() throws IOException {

        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream stream = loader.getResourceAsStream("config.properties");
            prop.load(stream);
        } catch (Exception e) {

        }


        this.login = prop.getProperty("login");
        this.password = prop.getProperty("password");
        this.host = prop.getProperty("myHost");
    }

}
