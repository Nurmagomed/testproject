package com.example.webapplication.models.parametrs;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;


public class User {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String secondName;

    public User(String name, String secondName) {
        this.name = name;
        this.secondName = secondName;
    }

}
