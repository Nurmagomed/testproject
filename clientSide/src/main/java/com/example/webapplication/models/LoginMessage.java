package com.example.webapplication.models;

import com.example.webapplication.models.parametrs.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//формирует json объект
public class LoginMessage {

    Request request1 = new Request("name", "secondName", "body");
    Gson jsonMessage;

    public String setJson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return gson.toJson(request1);
    }

    public Gson getJson() {
        return jsonMessage;
    }

}
