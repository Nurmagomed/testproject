package com.example.webapplication.models;

import com.example.webapplication.models.parametrs.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//формирует json
public class OutMessage {

    Request request1 = new Request("name", "secondName", "body");
    String json;

    public String setJson(Request request) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return this.json = gson.toJson(request1);
    }

    public String getJson() {
        return json;
    }

}
