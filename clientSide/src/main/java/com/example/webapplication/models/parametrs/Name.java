package com.example.webapplication.models.parametrs;

import lombok.Getter;
import lombok.Setter;

public class Name {

//    @Getter
//    @Setter
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
