package com.example.webapplication.models.parametrs;

import lombok.Getter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {

    @Getter
    private String body;

    @Getter
    private String timestamp;

    public Message(String body) {
        this.body = body;
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH24:mm:ss");
        this.timestamp = df.format(date);
    }

}
