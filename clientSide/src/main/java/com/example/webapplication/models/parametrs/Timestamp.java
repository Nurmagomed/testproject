package com.example.webapplication.models.parametrs;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Timestamp {

    private String timestampe;

    public String getTimestampe() {
        return timestampe;
    }

    public void setTimestampe(String timestampe) {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH24:mm:ss");
        this.timestampe = df.format(date);
    }

}
