package com.example.webapplication;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GreetingController {

    @GetMapping("/")
    public String greeting() { return "sendMessage";}

    @GetMapping("/responce")
    public String message() { return  "responce";}

}
